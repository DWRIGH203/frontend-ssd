import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClientService, Ticket } from '../service/http-client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-ticket',
  templateUrl: './edit-ticket.component.html',
  styleUrls: ['./edit-ticket.component.css']
})
export class EditTicketComponent implements OnInit {

  ticketNumber: string;
  ticket: Ticket;
  constructor(private router: Router, private route: ActivatedRoute, private httpClientService: HttpClientService) {

    this.ticketNumber = this.route.snapshot.paramMap.get("ticketNumber");
    this.httpClientService.getTicketByName(this.ticketNumber).subscribe((response) => {
      this.ticket = response;
    });

  }

  ngOnInit() {
  }

}
