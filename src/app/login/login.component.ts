import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { HttpClientService, Ticket } from '../service/http-client.service';
import { AppService } from 'src/app/app.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username = ''
  password = ''
  invalidLogin = false
  specialCharError: boolean;

  constructor(private router: Router,
    private loginservice: AuthenticationService, private httpClientService: HttpClientService, private appService: AppService
  ) { }

  ngOnInit() {
  }

  checkLogin() {

    var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    if (!format.test(this.username) && !format.test(this.password)) {

      (this.loginservice.authenticate(this.username, this.password).subscribe(
        data => {
          this.router.navigate([''])
          this.invalidLogin = false
          this.appService.setUserLoggedIn(true)
        },
        error => {
          this.invalidLogin = true

        }));
    } else {
      console.log("potential xxs attack")
      this.specialCharError = true;
    }
  }

}