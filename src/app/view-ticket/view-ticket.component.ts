import { Component, OnInit, Input, TemplateRef, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClientService, Ticket } from '../service/http-client.service';

export class Comment {
  constructor(
    public name: string,
    public commentValue: string,
    public commentId: string,
    public ticketNumber: string,
    public timestamp: string,
  ) { }
}


@Component({
  selector: 'app-view-ticket',
  templateUrl: './view-ticket.component.html',
  styleUrls: ['./view-ticket.component.css']
})


export class ViewTicketComponent implements OnInit {

  ticketNumber: string;
  ticket: Ticket;
  ticketComments: Comment[];
  name: string;
  commentValue: string;
  extractedCommentId: string;
  generatedCommentId: string;
  username: string;
  timestamp: Date = new Date();
  dateString: string;
  closeResult: string;

  assignee: string
  description: string;
  bugFinder: string;
  status: string;
  type: string;
  priority: string;
  commentIds: Comment[];
  inputList: string[] = [];
  validationFailed: boolean = false;
  specialCharError: boolean;
  ticketNumberExists: boolean;
  allTickets: Ticket[];
  statusClosed: boolean;
  initialStatus: string;
  buttonDisabled: boolean;
  role: string;

  constructor(private route: ActivatedRoute, private httpClientService: HttpClientService) {

    this.ticketNumber = this.route.snapshot.paramMap.get("ticketNumber");

    this.httpClientService.getTicketComments(this.ticketNumber).subscribe((comments) => {
      this.ticketComments = comments;
    });

    this.httpClientService.getTicketByName(this.ticketNumber).subscribe((response) => {
      this.ticket = response;
      console.log(this.ticket.status + " ticket.status")
      this.initialStatus = this.ticket.status;

      if (this.ticket.status == 'Closed') {
        this.statusClosed = true;
      }

    });

  };

  ngOnInit() {
    this.username = sessionStorage.getItem('username')
    this.httpClientService.getUserRole(this.username).subscribe(
      role => {
        this.role = role.role;

        if (this.role == 'OTHER_USER') {
          this.buttonDisabled = true;
        }
      });
  }

  addComment(): void {

    this.inputList.push(this.assignee,
      this.status, this.priority);

    var commentFormat = /[#$^*_+\\[\]{}\\|<>\/]+/;

    if (this.commentValue.match(commentFormat)) {
      this.validationFailed = true;
    }

    if (this.validationFailed === false) {

      this.createCommentId();
      this.dateString = this.timestamp.toDateString()
      this.username = sessionStorage.getItem('username')

      var commentJson = {
        name: this.username,
        commentValue: this.commentValue,
        commentId: this.generatedCommentId,
        ticketNumber: this.ticketNumber,
        timestamp: this.dateString
      }

      this.httpClientService.addComment(commentJson).subscribe((response) => {
        alert("Comment added successfully.");
        location.reload();

      })
    } else {
      this.specialCharError = true;
    }
  }

  createCommentId() {
    if (this.ticketComments.length == 0) {
      this.generatedCommentId = (this.ticketNumber + "-1");
    } else {
      let extractedCommentId: any = this.ticketComments[this.ticketComments.length - 1].commentId;
      let number = extractedCommentId.split('-')[2];
      let commentNumber = (+number + 1) + "";

      this.generatedCommentId = this.ticketNumber + "-" + commentNumber;

    }
  }

  editTicket(ticket: Ticket): void {
    //Creates a list of all user inputs for validation
    this.inputList.push(this.assignee,
      this.status, this.priority);

    var i;
    for (i = 0; i < this.inputList.length; i++) {
      var format = /["'#$^&*()_+\=\[\]{}\\|<>\/?]+/;

      if (format.test(this.inputList[i])) {
        this.validationFailed = true;
      }
    }

    //Sends request if validation is successful
    if (this.validationFailed === false) {

      var ticketJson = {
        ticketNumber: this.ticketNumber,
        assignee: this.assignee,
        status: this.status,
        priority: this.priority,

      }
      this.httpClientService.editTicket(ticketJson)
        .subscribe(ticket => {
          alert("Ticket edited successfully.");
          location.reload();
        });

    } else {
      //Enabling error banner if validation fails
      this.specialCharError = true;
    }
  }
}

export class EditTicketComponent {
  constructor() { }
}
