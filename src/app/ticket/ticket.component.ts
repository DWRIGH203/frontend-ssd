import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { HttpClientService, Ticket, Role } from '../service/http-client.service';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.css']
})


export class TicketComponent implements OnInit {

  tickets: Ticket[];
  ticket: Ticket;
  public ticketNumber: string;
  public responseTicket: Ticket;
  show: boolean = false;
  buttonDisabled: boolean;
  adminDisabled: boolean;
  role: string;
  username: string;

  constructor(
    private httpClientService: HttpClientService, private loginService: AuthenticationService,
    private router: Router,


  ) { }

  ticketNumber1: string = this.ticketNumber;

  ngOnInit() {
    this.username = sessionStorage.getItem('username')
    this.httpClientService.getUserRole(this.username).subscribe(
      role => {
        this.role = role.role;

        if (this.role == 'OTHER_USER') {
          this.buttonDisabled = true;
        }

        if (this.role == 'OTHER_USER' || this.role == 'USER') {
          this.adminDisabled = true;
        }
      }

    )

    this.httpClientService.getAllTickets().subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  @Output() eventClicked = new EventEmitter<Event>();


  deleteTicket(ticketNumber: Ticket): void {
    if (confirm('Are you sure you want to delete this?')) {

      var ticketJson = {
        ticketNumber: ticketNumber,
      }

      this.httpClientService.deleteTicket(ticketJson)
        .subscribe(res => {
          this.show = true;
          location.reload();
        })
    }
  };


  viewTicket(ticketNumber: Ticket) {

    this.httpClientService.getTicketByName(ticketNumber).subscribe(
      response => {
        this.handleSuccess(response)
        this.responseTicket = response;

      }
    )

  };


  handleSuccessfulResponse(response) {
    this.tickets = response;
  }

  handleSuccess(response) {
    this.ticket = response;
  }

  closePopup() {
    this.show = false;
  }

  onClick(ticket: Ticket): void {
    this.router.navigate(['/viewTicket', ticket.ticketNumber]);
  }

}

export class ViewTicketComponent {

  constructor() { }

}
