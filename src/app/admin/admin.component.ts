import { Component, OnInit } from '@angular/core';
import { HttpClientService } from '../service/http-client.service';

export class User {
  constructor(
    public status: string,
    public username: string,
    public roleId: string,
  ) { }
}

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})

export class AdminComponent implements OnInit {

  username: string;
  role: string;
  adminDisabled: boolean = true;
  users: User[];


  constructor(private httpClientService: HttpClientService) {
    this.username = sessionStorage.getItem('username')
    this.httpClientService.getUserRole(this.username).subscribe(
      role => {
        this.role = role.role;

        if (this.role == 'ADMIN') {
          this.adminDisabled = false;
        }
      }

    )
  }

  ngOnInit() {
    this.httpClientService.getAllUsers().subscribe(
      users => {
        this.users = users;
      }
    )
  }

}
