import { Component, OnInit } from '@angular/core';
import { HttpClientService, Ticket } from '../service/http-client.service';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.css']
})
export class AddTicketComponent implements OnInit {

  ticketNumber: string;
  priority: string;
  assignee: string;
  type: string;
  role: string;
  buttonDisabled: boolean;
  username: string;
  description: string;
  bugFinder: string;
  status: string;
  inputList: string[] = [];
  validationFailed: boolean = false;
  specialCharError: boolean;
  ticketNumberExists: boolean;
  allTickets: Ticket[];
  dateString: string;
  timestamp: Date = new Date();


  constructor(
    private httpClientService: HttpClientService
  ) { }

  ngOnInit() {

    this.httpClientService.getAllTickets().subscribe(
      response => this.allTickets = response,
    );
    //Sets user role for add-ticket page
    this.username = sessionStorage.getItem('username')
    this.httpClientService.getUserRole(this.username).subscribe(
      role => {
        this.role = role.role;

        if (this.role == 'OTHER_USER') {
          this.buttonDisabled = true;
        }
      }

    )
  }

  addTicket(): void {

    //Prefixes ticketNumber
    var ticketNumberPrefix = this.type.charAt(0).toUpperCase();
    this.ticketNumber = ticketNumberPrefix + "-" + this.ticketNumber;

    var x;
    for (x = 0; x < this.allTickets.length; x++) {
      if (this.ticketNumber == this.allTickets[x].ticketNumber) {
        this.ticketNumberExists = true;
        this.ticketNumber = '';
        return
      }
    }

    //Creates a list of all user inputs for validation
    this.inputList.push(this.ticketNumber, this.assignee,
      this.description, this.bugFinder);

    var i;
    for (i = 0; i < this.inputList.length; i++) {
      var format = /[#$^&*()_+\=\[\]{}\\|<>\/?]+/;

      if (format.test(this.inputList[i])) {
        this.validationFailed = true;
      }
    }

    //Sends request if validation is successful
    if (this.validationFailed === false) {

      var date = this.timestamp.toDateString().split(' ').slice(1).join(' ');
      var time = this.timestamp.toLocaleTimeString([], { hour: '2-digit', minute: '2-digit' });
      this.dateString = date + " " + time;

      var ticketJson = {
        ticketNumber: this.ticketNumber,
        assignee: this.assignee,
        priority: this.priority,
        type: this.type,
        description: this.description,
        bugFinder: this.bugFinder,
        status: "Open",
        timestamp: this.dateString,

      }
      this.httpClientService.addTicket(ticketJson)
        .subscribe(ticket => {
          alert("Ticket created successfully.");
          location.reload();
        });

    } else {
      //Enabling error banner if validation fails
      this.specialCharError = true;
    }
  }
}
