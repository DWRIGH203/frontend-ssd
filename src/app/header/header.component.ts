import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../service/authentication.service';
import { HttpClientService } from '../service/http-client.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  buttonDisabled: boolean;
  username: string;
  role: string;


  constructor(private loginService: AuthenticationService, private httpClientService: HttpClientService) {
  }

  ngOnInit() {
    console.log("HEADER REFRESH")

    this.username = sessionStorage.getItem('username')
    this.httpClientService.getUserRole(this.username).subscribe(
      role => {
        this.role = role.role;

        if (this.role == 'OTHER_USER' || this.role == 'USER') {
          this.buttonDisabled = true;
        }
      });
  }

}