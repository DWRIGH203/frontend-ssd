import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export class Ticket {
  constructor(
    public ticketNumber: string,
    public timestamp: string,
    public description: string,
    public bugFinder: string,
    public assignee: string,
    public status: string,
    public priority: string,
    public type: string,
    public commentId: string[],
  ) { }
}

export class Comment {
  constructor(
    public name: string,
    public commentValue: string,
    public commentId: string,
    public ticketNumber: string,
    public timestamp: string,
  ) { }
}

export class Role {
  constructor(
    public roleId: string,
    public role: string,
  ) { }
}

export class User {
  constructor(
    public status: string,
    public username: string,
    public roleId: string,
  ) { }
}


@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  constructor(
    private httpClient: HttpClient
  ) {
  }


  getAllTickets() {
    return this.httpClient.get<Ticket[]>('https://localhost:8080/getAllTickets');
  }

  addTicket(ticketjson) {
    return this.httpClient.post('https://localhost:8080/addTicket', ticketjson);
  }

  deleteTicket(ticketjson) {
    return this.httpClient.post('https://localhost:8080/deleteTicket', ticketjson);
  }

  getTicketByName(ticketNumber) {
    return this.httpClient.get<Ticket>('https://localhost:8080/getTicketByName' + "/" + ticketNumber)
  }

  getTicketComments(ticketNumber) {
    return this.httpClient.get<Comment[]>('https://localhost:8080/getComments' + "/" + ticketNumber)
  }

  addComment(commentjson) {
    return this.httpClient.post('https://localhost:8080/addComment', commentjson);
  }

  getUserRole(username) {
    return this.httpClient.get<Role>('https://localhost:8080/getRole' + "/" + username)
  }

  editTicket(ticketjson) {
    return this.httpClient.put('https://localhost:8080/editTicket', ticketjson)
  }

  getAllUsers() {
    return this.httpClient.get<User[]>('https://localhost:8080/getAllUsers')
  }
}