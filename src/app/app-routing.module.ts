import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { AuthGaurdService } from './service/auth-gaurd.service';
import { TicketComponent } from './ticket/ticket.component';
import { AddTicketComponent } from './add-ticket/add-ticket.component';
import { ViewTicketComponent } from './view-ticket/view-ticket.component';
import { EditTicketComponent } from './edit-ticket/edit-ticket.component';
import { AdminComponent } from './admin/admin.component';



const routes: Routes = [
  { path: '', component: TicketComponent, canActivate: [AuthGaurdService] },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGaurdService] },
  { path: 'addTicket', component: AddTicketComponent, canActivate: [AuthGaurdService] },
  { path: 'viewTicket/:ticketNumber', component: ViewTicketComponent, canActivate: [AuthGaurdService] },
  { path: 'editTicket/:ticketNumber', component: EditTicketComponent, canActivate: [AuthGaurdService] },
  { path: 'admin', component: AdminComponent, canActivate: [AuthGaurdService] },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
